# My Investments

My Investments is an app to track your investments based on your frequency.

---

My Investments in multiple stacks

1. React + localStorage
2. React + Redux - [Link](https://gitlab.com/chetanraj/my-investments-react/tree/redux)
3. React + mongoDB - [Link](https://gitlab.com/chetanraj/my-investments-react/tree/mongodb)
4. React + Firebase
5. React + diskDB
