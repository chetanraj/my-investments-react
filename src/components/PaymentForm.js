import React, { Component } from 'react';
import uuidv1 from 'uuid/v1'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const DB = 'my-investments';

class PaymentForm extends Component {
  constructor(props) {
    super(props);

    //! To maintain the state
    this.state = {
      type: '',
      amount: '',
      frequency: ''
    };
  }

  handleSubmit = event => {
    event.preventDefault();

    //! 
    if (!this.state.type || !this.state.amount || !this.state.frequency) {
      toast.error("Please enter some data !", { position: toast.POSITION.TOP_CENTER });
      return;
    }

    let investment = {};
    investment.id = uuidv1();
    investment.type = this.state.type;
    investment.amount = this.state.amount;
    investment.frequency = this.state.frequency;

    let investments = window.localStorage.getItem(DB);
    if(investments) {
      investments = JSON.parse(investments);
    } else {
      investments = [];
    }

    this.setState({ type: "", amount: "", frequency: "" });

    investments.push(investment);

    // Trigger onPaymentCardAdded
    this.props.onPaymentCardAdded(investments);

    // TODO Compare the previous and new object and then add to localStorage

    window.localStorage.setItem(DB, JSON.stringify(investments));

    toast.success("Yayy 🎉", { position: toast.POSITION.TOP_CENTER });
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  render() {
    let {type, amount, frequency} = this.state;
    const { render } = this.props;

    return <div className="payment-form">
      {render("Add an Investment")}
      <form onSubmit={this.handleSubmit}>
        <div className="row">
          <div className="col-12 col-sm-4 col-md-3">
            <div className="form-group">
              <label htmlFor="type">Type</label>
              <input type="text" className="form-control" id="type" value={type} onChange={this.handleChange} placeholder="For ex. Insurance" />
            </div>
          </div>

          <div className="col-12 col-sm-4 col-md-3">
            <div className="form-group">
              <label htmlFor="amount">Amount</label>
              <input type="number" className="form-control" id="amount" value={amount} onChange={this.handleChange} placeholder="For ex. 10000" />
            </div>
          </div>

          <div className="col-12 col-sm-4 col-md-3">
            <div className="form-group">
              <label htmlFor="frequency">Frequency</label>
              <select id="frequency" value={frequency} onChange={this.handleChange} className="form-control custom-select">
                <option value="">Select One</option>
                <option value="0.03">Daily</option>
                <option value="0.25">Weekly</option>
                <option value="0.5">Fortnightly</option>
                <option value="1">Monthly</option>
                <option value="3">Quarterly</option>
                <option value="6">Half Yearly</option>
                <option value="12">Yearly</option>
                <option value="365">One time</option>
              </select>
            </div>
          </div>

          <div className="col-12 col-sm-12 col-md-3 align-self-center add-investment">
            <button type="submit" className="btn btn-primary col-12">
              Add Investment
                  </button>
          </div>
        </div>
        <ToastContainer />
      </form>
    </div>;
  }
}

export default PaymentForm;