import React, { Component } from "react";
import styled, { keyframes } from "styled-components";

//! Styled Components
const BounceAnimation = keyframes`
  0% { margin-bottom: 0; }
  50% { margin-bottom: 15px }
  100% { margin-bottom: 0 }
`;

const DotWrapper = styled.div`
  height: 100vh;
  width: 100vw;
  background: #fff;
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 1;
`;

const Dot = styled.div`
  background-color: #2b3595;
  border-radius: 50%;
  width: 10px;
  height: 10px;
  margin: 0 5px;

  /* Animation */
  animation: ${BounceAnimation} 0.5s linear infinite;
  animation-delay: ${props => props.delay};
`;

const Label = styled.div`
  font-size: 25px;
  color: #2b3595;
  margin-right: 0.2em;
  margin-bottom: 0.4em;
`;

class LoadingDots extends Component {

    constructor(props) {
        super(props);

        //* State
        this.state = {
            label: ''
        }
    }

    render() {
        return (
            <DotWrapper className={ `dot-wrapper ${this.props.classToShow}` }>
                <Label>{this.props.label}</Label>
                <Dot delay="0s" />
                <Dot delay=".1s" />
                <Dot delay=".2s" />
            </DotWrapper>
        );
    }
}

export default LoadingDots;
