import React, { Component } from 'react';
import Card from 'card-vibes';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import posed from "react-pose";

const Button = posed.div({
    pressable: true,
    init: { scale: 1 },
    press: { scale: 1.1 }
});

const DB = 'my-investments';

class Cards extends Component {
    constructor(props) {
        super(props);

        //! To maintain the state
    	this.state = {
            frequency: {
                "0.03": "Daily",
                "0.25": "Weekly",
                "0.5": "Fortnightly",
                "1": "Monthly",
                "3": "Quarterly",
                "6": "Half Yearly",
                "12": "Yearly",
                "365": "One time"
            }
        };
    }
    
    componentDidMount() {
        
    }

    removeInvestment(id) {
        let investments = this.props.cards;
        investments = investments.filter(investment => investment.id !== id);

        let self = this;

        //* Parse before setting to localStorage
        localStorage.setItem(DB, JSON.stringify(investments));

        self.props.onPaymentCardRemoval(investments);

        toast.error("No 😞", { position: toast.POSITION.TOP_CENTER });
    }

    formatAmount(amount) {      
      return Number(amount).toLocaleString('en-IN');
    }

    render() {
        let cards = this.props.cards;
        
        const Cards = cards.map(card => (
          <Card
            style={{
              width: "100%",
              padding: "1em",
              borderRadius: "4px",
              marginBottom: "2em"
            }}
            className="float-left"
            key={card.id}
          >
            <h4>{card.type}</h4>
            {this.state.frequency[card.frequency]} of ₹{this.formatAmount(card.amount)}
            <div className="form-row text-center">
              <div className="col-12">
                <Button>
                  <button
                    onClick={() => this.removeInvestment(card.id)}
                    className="btn btn-info mt-4 mb-2 col-12"
                  >
                    Remove this card
                  </button>
                </Button>
              </div>
            </div>
          </Card>
        ));

        return (
            <div className="Cards">{Cards}</div>
        );
    }
  }
  
  export default Cards;