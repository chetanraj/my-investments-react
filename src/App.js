import React, { Component } from 'react';
import './css/App.css';

//! Components
import PaymentForm from './components/PaymentForm';
import Cards from './components/Cards';
import LoadingDots from "./components/LoadingDots";

//! Constants
const DB = 'my-investments';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      label: "Loading your investments",
      classToShow: "",
      cards: JSON.parse(window.localStorage.getItem(DB)) || []
    };
  }

  componentDidMount() {
    let self = this;

    setTimeout(() => {
      self.setState({ classToShow: "hidden" });
    }, 2000);
  }

  onPaymentCardAdded (investments) {
    this.setState({
      cards: investments
    });
  }

  onPaymentCardRemoval (investments) {
    this.setState({
      cards: investments
    })
  }

  render() {
    return (
      <div className="App">
        <LoadingDots
          label={this.state.label}
          classToShow={this.state.classToShow}
        />

        <nav className="navbar navbar-expand-lg">
          <div className="container-fluid">
            <div className="navbar-header col-12 text-center">
              <span className="navbar-brand">My Investments</span>
            </div>
          </div>
        </nav>
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 p-4">
              <PaymentForm
                cards={this.state.cards}
                onPaymentCardAdded={this.onPaymentCardAdded.bind(this)}
                render={headerLabel => <h5 className="mb-4">{headerLabel}</h5>}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-12 p-4">
              <Cards
                cards={this.state.cards}
                onPaymentCardRemoval={this.onPaymentCardRemoval.bind(this)}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
